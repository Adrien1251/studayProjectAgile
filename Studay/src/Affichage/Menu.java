package Affichage;

import java.util.GregorianCalendar;
import java.util.Scanner;

import CSV.CSV;
import calend.Calendrier;
import calend.EasyDate;
import calend.Evenement;

public class Menu {
	EasyDate date;
	String login;
	Calendrier cal;
	boolean fin = true;

	public Menu(String login) {
		this.login = login;
		cal = new Calendrier(login);
		while (fin) {
			AfficherMenu();
			fonctM();
		}
	}

	private void AfficherMenu() {
		System.out.println("||=======================================||");
		System.out.println("||           \033[36m     StuDay     \033[37m            ||");
		System.out.println("||       \033[34m  L'agenda de l'IUT'ay  \033[37m        ||");
		System.out.println("||=======================================||");
		System.out.println("||   1 : Saisir un evenement             ||");
		System.out.println("||   2 : Rechercher un evenement         ||");
		System.out.println("||   3 : Afficher une date               ||");
		System.out.println("||   4 : Afficher la semaine courante    ||");
		System.out.println("||   5 : Deconnexion                     ||");
		System.out.println("||=======================================||");
		System.out.println("||      \033[34m   Saisissez une action   \033[37m       ||");
		System.out.println("||=======================================||");
	}

	private void fonctM() {
		boolean drap = false;
		Scanner c = new Scanner(System.in);
		String i = c.nextLine();
		while (i.charAt(0) < '1' || i.charAt(0) > '5' || i.length() != 1) {
			System.out.println("||\033[31m  Entree invalide \033[37m " );
			i = c.nextLine();

		}
		int y = Integer.parseInt(i);
		if (y == 1) {
			String degre = "4";
			String importance = "";
			date = new EasyDate();
			while (date.date == null) {
				System.out.println("|| \033[34m Quelle est la date de votre evenement de l'annee 2016: \033[36m  (sous forme jj/mm/2016) \033[37m");
				String message = c.nextLine();
				if (message.equals("q") || message.equals("Q")) {
					drap = true;
					break;
				}
				date = new EasyDate(message);
			}
			if (drap == false) {

				System.out.println("|| \033[34m Quel nom souhaitez-vous pour votre evenement ? \033[37m ");
				String nomEvent = c.nextLine();
				if (nomEvent.equals("q") || nomEvent.equals("Q")) {
					drap = true;
				}
				if (drap == false) {
					while (degre.charAt(0) < '1' || degre.charAt(0) > '3' || degre.length() != 1) {
						System.out.println("||===============================||");
						System.out.println("||  Importance de l'evenement :  ||");
						System.out.println("||===============================||");
						System.out.println("||  1 : \033[32m Peu Important \033[37m          ||");
						System.out.println("||  2 : \033[33m Important \033[37m              ||");
						System.out.println("||  3 : \033[31m Tres Important \033[37m         ||");
						System.out.println("||===============================||");
						System.out.println("||    \033[34m  Saisissez une action  \033[37m   ||");
						System.out.println("||===============================||");

						degre = c.nextLine();
						if (degre.equals("q") || degre.equals("Q")) {
							drap = true;
							break;
						}
					}
					if (drap == false) {
						int deg = Integer.parseInt(degre);
						if (deg == 1) {
							importance = "\033[32m Peu Important\033[37m";
						} else if (deg == 2) {
							importance = "\033[33m Important\033[37m";
						} else if (deg == 3) {
							importance = "\033[31m Tres Important\033[37m";
						}
						cal.getCalendar(date.date).addEvent(new Evenement(nomEvent, deg));
						System.out.println(importance);
						System.out.println("|| \033[34m  Vous venez d'enregistrer un evenement " + importance + " \033[34m  pour le " + "\033[36m " + date
								+ " (" + nomEvent + ") \033[37m ");

						try {
							Thread.sleep(2500);
						} catch (InterruptedException ex) {
							Thread.currentThread().interrupt();
						}
					}
				}
			}
		} else if (y == 2) {
			System.out.println("|| Quel evenement recherchez vous?");
			String event = c.nextLine();
			EasyDate enCours;
			EasyDate dateTrouve;
			boolean trouve = false;
			int d = 0;
			while (!trouve && d < cal.calendrier.size()) {
				enCours = cal.calendrier.get(d);
				for (Evenement evenement : enCours.getEvents()) {
					if (evenement.getNom().equals(event)) {
						dateTrouve = enCours;
						System.out.println("||" + event + ", a lieu le : " + dateTrouve);
						trouve = true;
					}
				}
				d++;
			}
			if (!trouve)
				System.out.println("|| Evenement non trouve!");
			try {
				Thread.sleep(2500);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		} else if (y == 4) {
			cal.getNbreEvent(new EasyDate(new GregorianCalendar()), 7);
			try {
				Thread.sleep(2500);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}

		} else if (y == 3) {
			System.out.println("|| Quelle date voulez vous consulter?");
			String dateEntree = c.nextLine();
			EasyDate dateEntreeCalendar = new EasyDate(dateEntree);
			EasyDate enCours;
			boolean trouve = false;
			int d = 0;
			while (!trouve && d < cal.calendrier.size()) {
				enCours = cal.calendrier.get(d);
				if (enCours.date.equals(dateEntreeCalendar.date)) {
					System.out.println("|| Le " + dateEntree + " a lieu les evenements suivants : " + enCours.getEvents());
					trouve = true;
				}
				d++;
			}
			if (!trouve)
				System.out.println("Evenement non trouve!");
		} else if (y == 5) {
			fin = false;
			CSV.ajouterDate(cal.calendrier, login);
			System.out.println("|| Au revoir :)");
		}

	}
}
