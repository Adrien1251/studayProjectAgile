package login;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Inscription {
	public void inscription(){
		String choix = "o", mdp = "1", verifMdp = "2", login = "";
		Scanner sc = new Scanner(System.in);
		while(choix.equals("o") || choix.equals("O")){
			System.out.println("|| Saisissez votre login : ");
			login = sc.next();
			if(regarderSiLoginPrit(login)){
				System.out.println("|| Ce login est deja pris.");
			} else{
				System.out.println("|| "+login+" est votre login, voulez vous changer? (O / N)");
				choix = sc.next();
				System.out.println(choix);
			}
		}
		while(!mdp.equals(verifMdp)){
			System.out.println("|| Choisissez votre MDP : ");
			mdp = sc.next();
			System.out.println("|| Verification du MDP : ");
			verifMdp = sc.next();
			if(!mdp.equals(verifMdp)){
				System.out.println("|| Les deux MDP ne correspondent pas.");
			}
		}
		File fichier = new File("fichier/inscription.csv");
		try {
			FileWriter writer = new FileWriter(fichier, true);
			PrintWriter pw = new PrintWriter(writer);
			pw.print(login+",");
			pw.println(mdp);
			pw.close();
			writer.close();
			//crée le fichier
			BufferedWriter writer1 = new BufferedWriter(new FileWriter(new File("fichier/"+login+"csv.csv")));
			
			
			FileWriter writer2 = new FileWriter(new File("fichier/"+login+"csv.csv"), true);
			PrintWriter pw2 = new PrintWriter(writer2);
			pw2.print("01/01/2016,Jour de l'An, 1\n"
					+ "01/01/2016,Jour de l'An, 0\n"
					+"28/03/2016,Lundi de Pâques, 0\n"
					+"01/05/2016,Fête du Travail, 0\n"
					+"05/05/2016,Jeudi de l'Ascension, 0\n"
					+"08/05/2016,Armistice, 0\n"
					+"02/09/2016,Projet agile, 0\n"
					+"08/09/2016,Pas cours l'aprem, 0\n"
					+"24/12/2016,Noel, 0\n");
								
			pw2.close();
			writer2.close();
			writer1.close();
			
			
			System.out.println("|| Votre inscription a bien fonctionne");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean regarderSiLoginPrit(String login) {
		File fichier = new File("fichier/inscription.csv");
	
			BufferedReader bf;
			try {
				bf = new BufferedReader(new FileReader(fichier));
				String line = "";
				while((line = bf.readLine()) != null){
					for(int i=0; i<line.length(); i++){
						if(line.charAt(i) == ','){
							String nom = line.substring(0, i);
							if(nom.equals(login)){
								return true;
							}
						}
					}
				}
			
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	}
	
}
