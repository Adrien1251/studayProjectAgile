package login;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import Affichage.*;

public class Connexion {
	public String connexion(){
		Scanner sc = new Scanner(System.in);
	
		System.out.println("|| Entrez votre login : ");
		String login = sc.next();
		System.out.println("|| Entrez votre MDP : ");
		String mdp = sc.next();
		return regarderIdentifiant(login, mdp);
		

	}
	public String regarderIdentifiant(String login, String mdp){
		File identifiant = new File("fichier/inscription.csv");
		try {
			BufferedReader bf = new BufferedReader(new FileReader(identifiant));
			String line = "";
			while((line = bf.readLine()) != null){
				if(line.equals(login+","+mdp)){
					return login;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
