package CSV;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import calend.EasyDate;
import calend.Evenement;

public class CSV {
	public void ajouterDate(EasyDate date, String login){
		File fichier = new File("fichier/"+login+"csv.csv");
		FileWriter writer;
		try {
			writer = new FileWriter(fichier);
			for(Evenement nom : date.getEvents()){
				writer.write(date.getJour()+"/"+date.getMois()+"/"+date.getAnnee()+", "+nom.getNom()+"\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	static public void ajouterDate(ArrayList<EasyDate> date, String login){
		File fichier = new File("fichier/"+login+"csv.csv");
		FileWriter writer;
		try {
			writer = new FileWriter(fichier);
			for(EasyDate date1 : date){
				for(Evenement nom : date1.getEvents()){
					String jour = ""+date1.getJour();
					String mois = ""+date1.getMois();
					if(jour.length() == 1){
						jour = "0"+jour;
					} 
					if(mois.length() == 1){
						mois = "0"+mois;
					}
					
					writer.write(jour+"/"+mois+"/"+date1.getAnnee()+", "+nom.getNom()+", "+nom.getImportance()+"\n");
				}
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	static public ArrayList<EasyDate> sortieDesDates(String login){
		ArrayList<EasyDate> sortie = new ArrayList<EasyDate>();
		File fichier = new File("fichier/"+login+"csv.csv");
		try {
			BufferedReader bf = new BufferedReader(new FileReader(fichier));
			String evenement = "";
			while((evenement = bf.readLine()) != null){
				int nombreDeVIrgule = 0;
				int cpt = 0;
				while(cpt < evenement.length()){
					if(evenement.charAt(cpt) == ',' && nombreDeVIrgule==0){
						nombreDeVIrgule++;
						int jour, mois, annee;
						boolean dejaFait = false;
						jour = Integer.parseInt(evenement.substring(0, 2));
						mois = Integer.parseInt(evenement.substring(3, 5));
						annee = Integer.parseInt(evenement.substring(6, 10));

						for(EasyDate date1 : sortie){
							if(date1.getJour() == jour && date1.getMois() == mois && date1.getAnnee() == annee){
								dejaFait = true;
								for(int j=cpt+1; j<evenement.length(); j++){
									if(evenement.charAt(j) == ','){
										date1.addEvent(new Evenement(evenement.substring(cpt+1, j)));
									}
								}
							}
						}
						if(!dejaFait){
							EasyDate date = new EasyDate(evenement.substring(0, cpt));
							for(int j=cpt+1; j<evenement.length(); j++){
								if(evenement.charAt(j) == ','){
									date.addEvent(new Evenement(evenement.substring(cpt+1, j)));
								}
							}
							sortie.add(date);
						}
					}
					cpt++;
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sortie;
	}
}
