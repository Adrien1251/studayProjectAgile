package calend;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import CSV.CSV;

public class Calendrier {

	public ArrayList<EasyDate> calendrier = new ArrayList<EasyDate>();

	public Calendrier(String login) {
		ArrayList<EasyDate> datesCsv = CSV.sortieDesDates(login);
		for (int m = 0; m < 12; m++) {
			GregorianCalendar dateTampon = new GregorianCalendar();
			dateTampon.set(Calendar.MONTH, m);
			for (int j = 1; j <= dateTampon.getActualMaximum(Calendar.DAY_OF_MONTH); j++) {
				EasyDate date = new EasyDate(j, m, new GregorianCalendar().get(Calendar.YEAR));
				for (EasyDate dateeasy : datesCsv) {
					if (dateeasy.date.equals(date.date)) {
						date.addEvents(dateeasy.getEvents());
					}
				}
				calendrier.add(date);
			}
		}
	}

	public ArrayList<Evenement> getEvent(EasyDate date) {
		return date.getEvents();
	}

	public String getEventNom(EasyDate date) {
		String text = "";
		for (Evenement event : date.getEvents()) {
			text += event.getNom() + ", ";
		}
		return text;
	}

	public EasyDate getDate(String date) {
		EasyDate dateTampon = new EasyDate(date);
		for (EasyDate d : calendrier) {
			if (dateTampon.equals(d)) {
				return d;
			}
		}
		return null;
	}

	public EasyDate getCalendar(GregorianCalendar date) {
		GregorianCalendar dateTampon = new GregorianCalendar(date.get(Calendar.YEAR), date.get(Calendar.MONTH),
				date.get(Calendar.DAY_OF_MONTH));
		for (EasyDate d : calendrier) {
			if (dateTampon.equals(d.date)) {
				return d;
			}
		}
		return null;
	}

	public void getNbreEvent(EasyDate date, int nbre) {
		GregorianCalendar dateTampon = (GregorianCalendar) date.date.clone();
		for (int i = 0; i < nbre; i++) {
			System.out.println(this.getCalendar(dateTampon) + " : " + getCalendar(dateTampon).getEvents());
			dateTampon.add(Calendar.DAY_OF_MONTH, 1);
		}
	}

	public String toString() {
		String afficher = "";
		for (int i = 0; i < calendrier.size(); i++) {
			afficher = afficher + calendrier.get(i) + calendrier.get(i).getEvents() + "\n";
		}
		return afficher;
	}

}