package calend;

public class Evenement {

	private String nom;
	private int heure = 0;
	private int minute = 0;
	private int importance = 0;

	public String getNom() {
		return nom;
	}

	public String getHeure() {
		return heure + "h" + minute;
	}

	public int getImportance() {
		return importance;
	}

	public Evenement(String nom) {
		this.nom = nom;
	}

	public Evenement(String nom, int importance) {
		this(nom);
		this.importance = importance;
	}

	public Evenement(String nom, int heure, int minute) {
		this(nom);
		if (heure <= 23 && heure >= 0 && minute <= 59 && minute >= 0) {
			this.heure = heure;
			this.minute = minute;
		} else {
			System.out.println("heure saisie invalide, ajustee a 8h00");
		}
	}

	public String toString() {
		return "Evenement : " + nom;
	}
}
