package calend;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class EasyDate {

	public GregorianCalendar date;
	ArrayList<Evenement> events = new ArrayList<>();

	public EasyDate() {
	}

	public EasyDate(int jour, int mois) {
		date = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), mois, jour);
	}

	public EasyDate(int jour, int mois, int annee) {
		date = new GregorianCalendar(annee, mois, jour);
	}
	
	public EasyDate(GregorianCalendar date){
		this.date = date;
	}

	public EasyDate(String chaine) {
		if (chaine.length() == 10 && chaine.matches("\\d\\d.\\d\\d.\\d\\d\\d\\d")) {

			GregorianCalendar dateTampon = new GregorianCalendar();

			int jour = Integer.parseInt(chaine.substring(0, 2));
			int mois = Integer.parseInt(chaine.substring(3, 5)) - 1;
			int annee = Integer.parseInt(chaine.substring(6, 10));

			if (annee == 2016) {
				dateTampon.set(Calendar.YEAR, annee);
				if (mois >= 0 && mois < 12) {
					dateTampon.set(Calendar.MONTH, mois);
					if (jour > 0 && jour <= dateTampon.getActualMaximum(Calendar.DAY_OF_MONTH)) {
						date = new GregorianCalendar(annee, mois, jour);
					} else {
						System.out.println("Jour incorrect, date non renseignee");
					}
				} else {
					System.out.println("Mois incorrect, date non renseignee");
				}
			} else {
				System.out.println("Annee incorrect, date non renseignee");
			}
		} else {
			System.out.println("Format incorrect, date non renseignee");
		}
	}

	public void addEvent(Evenement event) {
		events.add(event);
		// ajouter au fichier csv
	}

	public void addEvents(ArrayList<Evenement> evenements) {
		for (Evenement event : evenements)
			events.add(event);
	}

	public ArrayList<Evenement> getEvents() {
		return events;
	}

	public int getJour() {
		return date.get(Calendar.DAY_OF_MONTH);
	}

	public int getMois() {
		return date.get(Calendar.MONTH) + 1;
	}

	public int getAnnee() {
		return date.get(Calendar.YEAR);
	}

	public String toString() {
		if (date == null) {
			return "Date incorrecte";
		}
		return date.get(Calendar.DAY_OF_MONTH) + "/" + (date.get(Calendar.MONTH) + 1) + "/" + date.get(Calendar.YEAR);
	}

	int compareTo(EasyDate dateAComparer) {
		return this.date.compareTo(dateAComparer.date);
	}

	boolean equals(EasyDate dateATester) {
		return this.date.equals(dateATester.date);
	}

}