import java.util.Scanner;

import Affichage.*;
import CSV.CSV;
import calend.EasyDate;
import login.Connexion;
import login.Inscription;
public class Hello {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		String fin = "n";
		while(fin != "3"){
			String choix = "afe";
			while(choix.charAt(0)<'1' || choix.charAt(0)>'3' || choix.length()!=1){
				System.out.println("\n\n\n\n");
				System.out.println("||==========================||");
				System.out.println("||          StuDay          ||");
				System.out.println("||==========================||");
				System.out.println("||   1 : Connexion          ||");
				System.out.println("||   2 : Inscription        ||");
				System.out.println("||   3 : Quitter            ||");
				System.out.println("||==========================||");

				choix = sc.next();
				if(choix.equals("3")){
					fin = "3";
				}
			}
			if(Integer.parseInt(choix) == 1){
				String choixQuitter = "n";
				Connexion connect = new Connexion();

				String login = connect.connexion();
				while(login == null){
					System.out.println("|| Login ou MDP incorrect ( Q pour quitter )");
					login = connect.connexion();
					if(login == null){
						System.out.println("|| Voulez vous reessayer? (O / N)");
						choixQuitter = sc.next();
						if(!choixQuitter.equals("o") && !choixQuitter.equals("O")){
							break;
						}
					}
				} 
				if(login != null){
					Menu m = new Menu(login);
				}
			} else if(Integer.parseInt(choix) == 2){
				new Inscription().inscription();
			}
		}
		System.out.println("|| Merci d'avoir utilise StuDay");
	}

}
