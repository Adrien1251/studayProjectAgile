package tests;
import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.*;
import calend.EasyDate;
import calend.Evenement;

public class TestEasyDate {

	EasyDate date = new EasyDate(03,01,1980);
	
	
	/*@Test
	// A faire
	public void testEasyDate(String chaine) {	
	}*/
	
	@Test
	public void testGetEvents(){
		ArrayList <Evenement> arraytest = new ArrayList <Evenement>();
		ArrayList <Evenement> arraytestvide = new ArrayList <Evenement>();
		EasyDate datest = new EasyDate(01,01,1990);
		Evenement eventest = new Evenement("test");
		datest.addEvent(eventest);
		arraytest.add(eventest);
		assertTrue(datest.getEvents().equals(arraytest));
		assertFalse(datest.getEvents().equals(arraytestvide));
	}
	
	@Test
	public void testGetJour(){
		assertEquals(date.getJour(),03);
		assertEquals(date.getJour(),3);
	}
	
	@Test
	public void testGetMois(){
		assertEquals(date.getMois(),1+1);
		assertEquals(date.getMois(),01+1);
	}
	
	@Test
	public void testGetAnnee(){
		assertEquals(date.getAnnee(),1980);
		assertFalse(date.getAnnee()==80);
	}
	
	@Test
	public void testToString(){
		EasyDate datenulle = new EasyDate();
		assertEquals(datenulle.toString(),"Date incorrecte");
		assertEquals(date.toString(),"3/2/1980");
	}
	
	
}
