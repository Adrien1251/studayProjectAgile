package tests;

import EventSaved.DefaultEvent;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestDefaultEvent {

	@Test
	public void testGetNom() {
		assertTrue(DefaultEvent.JOURDELAN.getNom().equals("Jour de l'An"));
		assertTrue(DefaultEvent.SAINTVALENTIN.getNom().equals("Saint Valentin"));
		assertTrue(DefaultEvent.PRINTEMPS.getNom().equals("Printemps"));
		assertTrue(DefaultEvent.FETEDUTRAVAIL.getNom().equals("Fete du Travail"));
		assertTrue(DefaultEvent.ARMISTICE1945.getNom().equals("Armistice 1945"));
		assertTrue(DefaultEvent.ETE.getNom().equals("Ete"));
		assertTrue(DefaultEvent.FETENATIONALE.getNom().equals("Fete Nationale"));
		assertTrue(DefaultEvent.ASSOMPTION.getNom().equals("Assomption"));
		assertTrue(DefaultEvent.AUTOMNE.getNom().equals("Automne"));
		assertTrue(DefaultEvent.HALLOWEEN.getNom().equals("Halloween"));
		assertTrue(DefaultEvent.TOUSSAINT.getNom().equals("Toussaint"));
		assertTrue(DefaultEvent.ARMISTICE1918.getNom().equals("Armistice 1918"));
		assertTrue(DefaultEvent.SAINTECATHERINE.getNom().equals("Sainte Catherine"));
		assertTrue(DefaultEvent.SAINTNICOLAS.getNom().equals("Saint Nicolas"));
		assertTrue(DefaultEvent.HIVER.getNom().equals("Hiver"));
		assertTrue(DefaultEvent.NOEL.getNom().equals("Noel"));
	}

}
