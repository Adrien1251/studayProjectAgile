package EventSaved;

import calend.EasyDate;
import calend.Evenement;

public class GenerDefaultEvent {
	
	Evenement event;
	
	public GenerDefaultEvent(){}
	
	public Evenement getEvent(){return this.event;}
	
	public GenerDefaultEvent(EasyDate date){
		
		if (date.getMois()==1 && date.getJour()==1){
			event = new Evenement(DefaultEvent.JOURDELAN.getNom());
		} 
		else if (date.getMois()==2 && date.getJour()==14){
			event = new Evenement(DefaultEvent.SAINTVALENTIN.getNom());
		}
		else if (date.getMois()==3 && date.getJour()==20){
			event = new Evenement(DefaultEvent.PRINTEMPS.getNom());
		}
		else if (date.getMois()==5 && date.getJour()==1){
			event = new Evenement(DefaultEvent.FETEDUTRAVAIL.getNom());
		}
		else if (date.getMois()==5 && date.getJour()==8){
			event = new Evenement(DefaultEvent.ARMISTICE1945.getNom());
		}
		else if (date.getMois()==6 && date.getJour()==21){
			event = new Evenement(DefaultEvent.ETE.getNom());
		}
		else if (date.getMois()==7 && date.getJour()==14){
			event = new Evenement(DefaultEvent.FETENATIONALE.getNom());
		}
		else if (date.getMois()==8 && date.getJour()==15){
			event = new Evenement(DefaultEvent.ASSOMPTION.getNom());
		}
		else if (date.getMois()==9 && date.getJour()==23){
			event = new Evenement(DefaultEvent.AUTOMNE.getNom());
		}
		else if (date.getMois()==10 && date.getJour()==30){
			event = new Evenement(DefaultEvent.HALLOWEEN.getNom());
		}
		else if (date.getMois()==11 && date.getJour()==1){
			event = new Evenement(DefaultEvent.TOUSSAINT.getNom());
		}
		else if (date.getMois()==11 && date.getJour()==11){
			event = new Evenement(DefaultEvent.ARMISTICE1918.getNom());
		}
		else if (date.getMois()==11 && date.getJour()==25){
			event = new Evenement(DefaultEvent.SAINTECATHERINE.getNom());
		}
		else if (date.getMois()==12 && date.getJour()==6){
			event = new Evenement(DefaultEvent.SAINTNICOLAS.getNom());
		}
		else if (date.getMois()==12 && date.getJour()==21){
			event = new Evenement(DefaultEvent.HIVER.getNom());
		}
		else if (date.getMois()==12 && date.getJour()==25){
			event = new Evenement(DefaultEvent.NOEL.getNom());
		}
	}

}
